#!/usr/bin/env python

import struct

_T = 15  #THIS IS A FIXED PARAMETER, DO NOT MODIFY _T IN THE CODE
        #   minimum degree - least number of children in each node
        #   each node has at least _T-1 keys and _T children
        #   and at most 2*_T-1 keys and 2*_T children
_STRUCT_FMT = str(6*_T) + 'i?'

_USER_INDEX = 'user_index_data.bin'
_QUESTION_INDEX = 'question_index_data.bin'

class btNode:    
    def __init__(self, n, leaf, filename):
        self.n = n
        self.leaf = leaf 
        self.offset = None
        self.keys = []
        self.p_dump = []
        self.children = []
        self.filename = filename
    
    def __str__(self):
        s = str(self.n) + str(self.leaf) + str(self.offset) + str(self.keys) + str(self.p_dump) + str(self.chidlren)
        return s

    def allocate(self):
        #find node offset in data file
        self.offset = self.offset_append()
        #fill keys[] and children[] with maximum number of elements
        for i in range(2*_T-1):  #each node has at most 2t-1 keys and p_dump
            self.keys.append(-1)
            self.p_dump.append(-1)
        for i in range(2*_T):    #and 2*t children
            self.children.append(-1)
    
    def offset_append(self):
        try:
            fi = open(self.filename, 'rb')
            fi.seek(0,2)
            file_end = fi.tell()
            last_index = file_end / struct.calcsize(_STRUCT_FMT)
            fi.close()
        except IOError:
            last_index = 0
        return last_index

class bt:
    def __init__(self, root, filename):
        self.root = root    #root is an instance of btNode
        self.filename = filename

    def search(self, node, k):
        i = 0;
        while i<node.n and k>node.keys[i]:
            i += 1
        if i<node.n and k==node.keys[i]:
                tup = (node.offset, i)
                return tup
        elif node.leaf:
            return None
        else:
            child = disk_read(node.children[i], self.filename)
            return self.search(child, k)

    def split_child(self, node, i):
        inf_node = disk_read(node.children[i], self.filename) #reads children[i] from file to main memory
        sup_node = btNode(_T-1, inf_node.leaf, self.filename)
        sup_node.allocate()
        disk_write(sup_node)
        
        for j in range(_T-1):
            sup_node.keys[j] = inf_node.keys[j+_T]
            sup_node.p_dump[j] = inf_node.p_dump[j+_T]
        if not inf_node.leaf:
            for j in range(_T):
                sup_node.children[j] = inf_node.children[j+_T]
        inf_node.n = _T-1
        for j in range(node.n, i-1, -1):
            node.children[j+1] = node.children[j]
        node.children[i+1] = sup_node.offset
        for j in range(node.n-1, i-1, -1):
            node.keys[j+1] = node.keys[j]
            node.p_dump[j+1] = node.p_dump[j]
        node.keys[i] = inf_node.keys[_T-1]
        node.p_dump[i] = inf_node.p_dump[_T-1]
        node.n += 1
        
        disk_write(inf_node)
        disk_write(sup_node)
        disk_write(node)

    def insert(self, k, p_dump):
        r = self.root
        if r.n==2*_T-1:   #tests if node is full
            new_root = btNode(0, False, self.filename)
            new_root.allocate()
            new_root.children[0] = r.offset
            disk_write(new_root)
            self.root = new_root
            self.split_child(new_root, 0)
            self.insert_nonfull(new_root, k, p_dump)
            
        else:   
            self.insert_nonfull(r, k, p_dump)

    def insert_nonfull(self, node, k, p_dump):
        i = node.n - 1
        if node.leaf:
            while i>=0 and k<node.keys[i]:
                node.keys[i+1] = node.keys[i]
                node.p_dump[i+1] = node.p_dump[i]
                i -= 1
            node.keys[i+1] = k
            node.p_dump[i+1] = p_dump
            node.n += 1
            disk_write(node)
        else:
            while i>=0 and k<node.keys[i]:
                i -= 1
            i += 1
            node_child = disk_read(node.children[i], self.filename)
            if node_child.n==2*_T-1:
                self.split_child(node, i)
                if k>node.keys[i]:
                    i += 1
            node_child = disk_read(node.children[i], self.filename)
            self.insert_nonfull(node_child, k, p_dump)

    def in_order(self, node):
        if node.leaf:
            for i in range(node.n):
                print node.p_dump[i]
            return
        for i in range(node.n):
            node_child = disk_read(node.children[i], self.filename)
            self.in_order(node_child)
            print node.p_dump[i]
        last_child = disk_read(node.children[node.n], self.filename)
        self.in_order(last_child)

def disk_read(offset_read, filename):
    node_read = btNode(None, None, filename)
    node_read.allocate()

    index_fi = open(filename, 'rb')
    index_fi.seek(offset_read * struct.calcsize(_STRUCT_FMT))
    node_unpacked = struct.unpack(_STRUCT_FMT, index_fi.read(struct.calcsize(_STRUCT_FMT))) 
    index_fi.close()

    k = 0
    c = 0
    p = 0
    for i in range(6*_T-1):
        if i==0:
            node_read.n = node_unpacked[i]
        elif i>0 and i<=2*_T-1:
            node_read.keys[k] = node_unpacked[i]
            k+=1
        elif i>2*_T-1 and i<=4*_T-2:
            node_read.p_dump[p] = node_unpacked[i]
            p+=1
        elif i>4*_T-2 and i<=6*_T-2:
            node_read.children[c] = node_unpacked[i]
            c+=1
    node_read.offset = node_unpacked[-2]
    node_read.leaf = node_unpacked[-1]
  
    return node_read

def disk_write(node):
    packed = struct.pack('i', node.n)
    for i in range(2*_T-1):
        packed += struct.pack('i', node.keys[i])
    for i in range(2*_T-1):
        packed += struct.pack('i', node.p_dump[i])
    for i in range(2*_T):
        packed += struct.pack('i', node.children[i])
    packed += struct.pack('i', node.offset)
    packed += struct.pack('?', node.leaf)
    
    index_fi = open(node.filename, 'rb+')
    index_fi.seek(node.offset*(struct.calcsize(_STRUCT_FMT)))
    index_fi.write(packed)
    index_fi.close()

if __name__ == '__main__':
    filename = _USER_INDEX

    index_fi = open(filename, 'wb')
    index_fi.close()

    x = btNode(0, True, filename)
    x.allocate()
    disk_write(x)
    t = bt(x, filename)

    for i in range (200):
        t.insert(i, i)
    t.in_order(t.root)
